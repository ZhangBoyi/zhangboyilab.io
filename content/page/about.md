---
title: Welcome to Boyi's log
comments: false
---

Hi there, welcome to my blog! Here I'll log the problems I've encountered in my expidition to building new things through coding. I'll document how I encountered the pitfalls, and how I got to the bottom and came back up stronger. 

I pray this log shall prove a valuable reference for myself and my fellow sailors who come across the similar courses.

The routes I've become familar with and where I'm continuously exploring are:

Computer Vision, OpenCV, Maching Learning, and iOS development.

Blessings on your endeavors!

Boyi
