---
title: Resolve "DSO missing from command line" error
date: 2020-09-14
tags: ["CUDA", "Linux", "cmake"]
---
I encountered the following error while building a project on Linux.

```bash
/usr/bin/ld: CMakeFiles/ingame.dir/media/sdcard/ingame_app/sdk/src/ingame/dnn_in
fer.cpp.o: undefined reference to symbol 'cudaDeviceReset@@libcudart.so.10.2'
//usr/local/cuda-10.2/targets/aarch64-linux/lib/libcudart.so.10.2: error adding 
symbols: DSO missing from command line
```

The solution was found from <a href="https://stackoverflow.com/questions/19901934/libpthread-so-0-error-adding-symbols-dso-missing-from-command-line" target="blank">this link</a>.

A not accepted answer with 24 ups says the following. applying the `export LDFLAGS="-Wl,--copy-dt-needed-entries"` helped me to suppress the error.

### Background

The DSO missing from command line message will be displayed when the linker does not find the required symbol with it's normal search but the symbol is available in one of the dependencies of a directly specified dynamic library.

In the past the linker considered symbols in dependencies of specified languages to be available. But that changed in some later version and now the linker enforces a more strict view of what is available. The message thus is intended to help with that transition.

### What to do?

#### If you are the maintainer of the software

You should solve this problem by making sure that all libraries that are needed to satisfy the needed symbols are directly specified on the linker command line. Also keep in mind that order often matters.

#### If you are just trying to compile the software

As a workaround it's possible to switch back to the more permissive view of what symbols are available by using the option `-Wl,--copy-dt-needed-entries`.

Common ways to inject this into a build are to export LDFLAGS before running `configure` or similar like this:
```bash
export LDFLAGS="-Wl,--copy-dt-needed-entries"
```
Sometimes passing `LDFLAGS="-Wl,--copy-dt-needed-entries"` directly to make might also work.
