---
title: Eigen Cheat Sheet
date: 2018-06-28
tags: ["Eigen", "C++", "CheatSheet"]
---

Eigen is a header-only, lightweight linear algebra library. It is released under MPL2 license, which is a weak copyleft license. Here's a cheat sheet on how to initialize matrices and vectors, and how to perform simple operations on them.
 
```cpp
// fixed size
typedef Matrix<float, 4, 4> Matrix4f;
typedef Matrix<int, 1, 2> RowVector2i;

// dynamic size
typedef Matrix<double, Dynamic, Dynamic> MatrixXd;
typedef Matrix<int, Dynamic, 1> VectorXi; // column vector

// The matrix is stored column-major

// constructors
Matrix3f a;
MatrixXf b;
MatrixXf a(10,15);
VectorXf b(30);

// constructors to initialize value of small fixed-size vectors
Vector4d c(5.0, 6.0, 7.0, 8.0);

// coefficient accesors
MatrixXd m(2,2);
m(0,0) = 3;
m(1,0) = 2.5;

VectorXd v(2);
v(0) = 4;
v(1) = v(0) - 1;

// comma-initialization
Matrix3f m;
m << 1, 2, 3,
     4, 5, 6,
     7, 8, 9;
std::cout << m;
/* prints:
1 2 3
4 5 6
7 8 9
*/

// resizing
MatrixXd m(2,5);
m.resize(4,3);
std::cout << "The matrix m is of size " << m.rows() << "x" << m.cols() << std::endl;
std::cout << "It has " << m.size() << " coefficients" << std::endl;
/* prints:
The matrix m is of size 4x3
It has 12 coefficients
*/

// matrix addition, subtraction, and multiplication
Matrix2d a;
a << 1, 2,
     3, 4;
MatrixXd b(2,2);
b << 2, 3,
     1, 4;
a + b;
a - b;
a += b;
a * b;
a = a * a; // no aliasing issue
// scalar multiplication
a * 2.5   // a + 2.5 will fail
    
// transpose
a.transpose();
a.transposeInPlace();  // a = a.transpose() would give aliasing issue

// vector dot product and cross product
Vector3d v(1,2,3);
Vector3d w(0,1,2);
v.dot(w);
v.cross(w);

// arithmetic reduction operations
a.sum();		// 10
a.prod();		// 24
a.mean();		// 2.5
a.minCoeff();	// 1
a.maxCoeff();	// 4
a.trace();		// 5
std::ptrdiff_t i, j;
a.minCoeff(&i, &j); // 0, 0

// coefficient-wize operation
// array is like matrix, but is for coefficient-wize operations
MatrixXf m(2,2);
MatrixXf n(2,2);
m << 1,2,
     3,4;
n << 5,6,
     7,8;
m.array() * n.array();
/* result:
5  12
21 32
*/
m.cwiseProduct(n);
/* result:
5  12
21 32
*/
m.array() + 4;
/* result:
5 6
7 8
*/

// Interfacing with raw buffers: the Map class
Map<MatrixXf> mf(pf,rows,columns);  // pf is a float* pointing to the array of memory
Map<const Vector4i> mi(pi);

int array[8];
for(int i = 0; i < 8; ++i) array[i] = i;
cout << "Column-major:\n" << Map<Matrix<int,2,4> >(array) << endl;
cout << "Row-major:\n" << Map<Matrix<int,2,4,RowMajor> >(array) << endl;
cout << "Row-major using stride:\n" << Map<Matrix<int,2,4>, Unaligned, Stride<1,4> >(array) << endl;
/* prints:
Column-major:
0 2 4 6
1 3 5 7
Row-major:
0 1 2 3
4 5 6 7
Row-major using stride:
0 1 2 3
4 5 6 7
*/

// Reductions, visitors and broadcasting
// https://eigen.tuxfamily.org/dox/group__TutorialReductionsVisitorsBroadcasting.html
```

