---
title: Face Alignment
date: 2018-06-01
tags: ["Machine_Learning"]
---
Face alignment answers the question of finding keypoints on a human face. The following videos introduce two methods of face alignment in English and Chinese.

In English

<iframe width="560" height="315" src="https://www.youtube.com/embed/SLLZr9IjPcE?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/OWNSc57k5Gc?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<br/> 
In Chinese

<iframe width="560" height="315" src="https://www.youtube.com/embed/qzmcU2XZmXI?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/nlXzewdag-E?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
