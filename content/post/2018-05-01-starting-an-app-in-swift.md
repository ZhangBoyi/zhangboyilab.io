---
title: Starting an App in Swift
date: 2018-05-01
tags: ["Swift", "iOS"]
---

This post describes the simple procedures to start a minimalistic app in Swift.

1. Launch XCode, and select _Create a new Xcode project_.

	![Create a new Xcode project](/post_img/2018-05-01-starting-an-app-in-swift/1.png "Create a new Xcode project")

2. Select _Single View App_ in the view that follows, and click next.

	![Single View App](/post_img/2018-05-01-starting-an-app-in-swift/2.png "Single View App")

3. Key in the _Product Name_ and other information and click next. If _Team_ is left as _None_, then this project cannot run on phones, but can only run on simulators.

	![Project Info](/post_img/2018-05-01-starting-an-app-in-swift/3.png "Project Info")

4. On the upper left corner of the IDE, pick a simulator (e.g. _iPhone 8 Plus_), and click the play button to build and run. It might take some time to boot the simulator. As the app runs on  the simulator, you will see a mostly white screen, and some log information at the bottom of the IDE.

	![IDE](/post_img/2018-05-01-starting-an-app-in-swift/4.png "IDE")

	![Simulator](/post_img/2018-05-01-starting-an-app-in-swift/5.png "Simulator")

That's it! You've started a minimalistic app in Swift, and had it running on a simulator. the `viewDidLoad()` in _ViewController.swift_ is the first function that gets called. You may add `print("view loaded.")` in this function, and see the message printed in the log.

You may follow <a href="https://youtu.be/aiXvvL1wNUc" target="blank">this video</a> to add buttons and let your app play music. To learn more about the Swift language, I recommend you watch <a href="http://www.newthinktank.com/2017/05/learn-swift-3-one-video/" target="blank">this video</a> and read its cheat sheet. 

Happy coding!
