---
title: OpenCV On CUDA
date: 2019-10-30
tags: ["OpenCV", "CUDA", "Computer_Vision"]
---

This is the take away from this <a href="http://on-demand.gputechconf.com/gtc/2013/webinar/gtc-express-opencv-baksheev.mp4" target="blank">video</a> accompanied by this <a href="http://on-demand.gputechconf.com/gtc/2013/webinar/gtc-express-itseez-opencv-webinar.pdf" target="blank">slide</a>. It talks about running OpenCV on CUDA GPU. This seminar is based on OpenCV 2.4. The API in Opencv 3.X is different. A few points worth noting before entering the code:

1. `GpuMat` is a padded image container, the upload/download operations are described in pages 15 - 17. Page 20 gives an example of template matching.

2. Concurrent operation with CUDA is described in pages 24 to 28. `CudaMem` is page-locked CPU memory for asynchronous data transfers. `Stream` is an OpenCV wrapper of cuda class `cudaStream_t`. It represents asynchronous queue of operations, frees up CPU.

3. The following is an example of converting `GpuMat` to common GPU format and writing custom CUDA code to swap the r and b channel of the image.


*swap_rb.cpp*

<div markdown="0">
<pre><code class="cpp">#include &ltopencv2/gpu/stream_accessor.hpp&gt

void swap_rb_caller(const PtrStepSz&ltuchar3&gt& src, trStep&ltuchar3&gt dst, cudaStream_t stream);

void swap_rb(const GpuMat& src, GpuMat& dst, Stream& stream = Stream::Null())  // convert GpuMat to cuda format to process
{
	CV_Assert(src.type() == CV_8UC3);
	dst.create(src.size(), src.type()); // create if not allocated yet
	cudaStream_t s = StreamAccessor::getStream(stream);
	swap_rb_caller(src, dst, s);
}
</pre></code></div>


*swap_rb.cu*

<div markdown="0">
<pre><code class="c">#include &ltopencv2/core/cuda_devptrs.hpp&gt

__global__ void swap_rb_kernel(const PtrStepSz&ltuchar3&gt src, PteStep&ltuchar3&gt dst)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;i
	
	if (x < src.cols && y < src.rows)
	{
 		uchar3 v = src(y, x); // Reads pixel in GPU memory. Valid! We are on GPU!
 		dst(y, x) = make_uchar3(v.z, v.y, v.x);
 	}
}

void swap_rb_caller(const PtrStepSz&ltuchar3&gt& src, PtrStep&ltuchar3&gt dst, cudaStream_t stream)
{
 	dim3 block(32, 8);
 	dim3 grid((src.cols + block.x - 1)/block.x, (src.rows + block.y - 1)/ block.y);

 	swap_rb_kernel&lt&lt&ltgrid, block, 0, stream&gt&gt&gt(src, dst);

 	if (stream == 0)
 		cudaDeviceSynchronize();
}
</pre></code></div>

