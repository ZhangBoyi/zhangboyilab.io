---
title: Customizing Code Highlighting
date: 2018-05-20
tags: ["html", "javascript", "css"]
---

I've been trying to customize the way code appears in [my blog](https://zhangboyi.gitlab.io/). My blog is based on hugo and hyde-hyde, which already has code highlighting based on highlight.js. I need to 

* __Add support for Swift__
* __Select my own display style__
* __Make inline code appear the same as block code__
* __Load custom highlight.js CSS__
* __Customize inline code border width and color__

Here is how I solved these problems.

## Add support for Swift

In markdown, inline code is enclosed by back-ticks \` and \`, and code block is enclosed by three back-ticks on each side \`\`\` and \`\`\`. Yet in order to have uncommon languages such as Swift supported, extra setting need to be done. 

To specify the lauguage of the code, add the name of the language after the three back-ticks. For example, for Swift, write the following in your markdown. 

````
```swift
override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    NSLog("View Loaded");
    
    let classOc = ClassOc();
    classOc.updateString();
    classOc.printString();
}
```
````

Yet the displayed code block looks like this

![Swift1](/post_img/2018-05-20-customizing-code-highlighting/1.png "Swift1")

This is because by default the theme only loads highlight.js's minimum version, which only supports the most common languages. To add support for Swift, add the following to _config.toml_ under `[params]` to guarantee the script for that language is also loaded. 

```
[params]
  highlightjslanguages = ["swift"]
```

Now the code block appears like this

![Swift2](/post_img/2018-05-20-customizing-code-highlighting/2.png "Swift2")

## Select my own display style

The code block is showing some highlighting for Swift, yet because the default style is not very rich, it is preferable to choose my own style. The demo of the 79 display styles to date of highlight.js can be found at [this page](https://highlightjs.org/static/demo/).

Say we want to choose style vs2015, we add the following to _config.toml_.

```
[params]
  highlightjslanguages = ["swift"]
  highlightjsstyle = "vs2015"
```

Now the code block and the inline code appears as follows

![Swift3](/post_img/2018-05-20-customizing-code-highlighting/3.png "Swift3")

## Make inline code appear the same as block code

We can see that the color is richer for swift in the code block, but the inline code's color doesn't get changed, and the inconsistency need to be addressed.

in _themes/hyde-hyde/layouts/partials/commenting.html_, we can see highlight.js loaded by the following code

```html
{{ if .Site.Params.highlightjs -}}
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
{{ range .Site.Params.highlightjslanguages }}
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/{{.}}.min.js"></script>
{{ end }}
<script type="text/javascript">
    hljs.configure({languages: []});
    hljs.initHighlightingOnLoad();
</script>
```

By loading highlight.js in this default manner, it will only be applyed to \<pre\>\<code\>, which is equavalent to \`\`\`. As \` is translated as \<code\>, highlight.js is not applied to it. To apply highlight.js to \<code\>, we have to apply it in a different way. Following [this discussion](https://github.com/isagalaev/highlight.js/issues/945), I load highlight.js differently using jQuery

```html
{{ if .Site.Params.highlightjs -}}
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
{{ range .Site.Params.highlightjslanguages }}
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/{{.}}.min.js"></script>
{{ end }}
<script type="text/javascript">
    hljs.configure({languages: []});
    //hljs.initHighlightingOnLoad();
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
	  $('code').each(function(i, block) {
	    hljs.highlightBlock(block);
	  });
	  $('code').css({'display': 'inline'});
	  $('pre code').css({'display': 'block'});
	});
</script>
```

The css part is to make sure the inline section is not displayed as a block. With this, the code block appears as follows:

![Swift4](/post_img/2018-05-20-customizing-code-highlighting/4.png "Swift4")

## Load custom highlight.js CSS

In order to display our custom highlight.js style, we need to load the custom CSS file. Let's download a CSS file from [github](https://github.com/isagalaev/highlight.js/tree/master/src/styles), say _arduino-light.css_, and rename it as _arduino-light-boyi.css_ and put it under _themes/hyde-hyde/static/css/_. To load this file, we modify _themes/hyde-hyde/layouts/partials/header.html_ as follows

```html
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/{{ .Site.Params.highlightjsstyle | default "default" }}.min.css"> -->
    <link rel="stylesheet" href="/css/arduino-light.css">
```

We commented the line to load CSS file from online and replaced it with our custom CSS file. The style now appears as follows:

![Swift5](/post_img/2018-05-20-customizing-code-highlighting/5.png "Swift5")

## Customize inline code border width and color

It can be obeserved that the border of the inline code is too wide. To tune it smaller, we modify _themes/hyde-hyde/static/css/arduino-light-boyi.css_ by changing the value of padding. 

```css
.hljs {
  display: block;
  overflow-x: auto;
  padding: 0.2em;
  background: #fafafa;
}

pre code.hljs {
  padding: 0.5em;
}
```

I also changed the background's value from #ffffff to #fafafa to make the color lightgray. Since the comment's color is also lightgray, we need to increase its contrast with the background by changing its color and font. We do this by modifying 

```css
.hljs-comment {
  color: #a0a1a7;
  font-style: italic;
}
```

Further more, to make the inline code's font size consistent to the block code, I changed the font-size to 81%

```css
code {
    font-family: 'Roboto Mono', monospace;
    font-size: 81%;
}
```

Finally, the finished web page appear like this.

![Swift6](/post_img/2018-05-20-customizing-code-highlighting/6.png "Swift6")

