---
title: Using Nsight Eclipse to Profile Jetson Xavier Remotely
date: 2019-11-29
tags: ["CUDA", "Linux"]
---
This blog talks about how to run and profile a program on Jetson Xavier remotely. The steps are

* __Install Nsight Eclipse__
* __Set SSH Root Login__
* __Run Profiler__

## Install Nsight Eclipse
Nsight Eclipse is installed using Nvidia SDK Manager. When installing Jetpack on Xavier, make sure to also select the host machine to install Jetpack on the host PC. After this, Nsight Eclipse Edition can be found and executed from the Application folder.

One common obstacle from launching Nsight Eclipse Edition successfully is the Java version. The default Java version is 11 on Ubuntu, but Nsight Eclipse requires Java 8, the current Long Term Supported version. The following command will reveal which version is in use,

```bash
$ java -version
```

If the version is not right, version 8 can be installed with

```bash
$ sudo apt install openjdk-8-jdk
```

To select version 8, 

```bash
$ sudo update-alternatives --config java
```

and select the corresponding number.

## Set SSH Root Login
If we skip this step, we will still be able to run program remotely on Xavier, but we won't be able to debug or profile it because tools like *cuda-gdb* and *nvprof* require root previliage on Xavier to be launched. Debugging remotely without this step will result in hanging at

```
Reading /usr/lib/aarch64-linux-gnu/tegra/libnvidia-fatbinaryloader.so.debug from remote target...
Reading /usr/lib/aarch64-linux-gnu/tegra/.debug/libnvidia-fatbinaryloader.so.debug from remote target...
Reading /usr/lib/aarch64-linux-gnu/tegra/libnvos.so.debug from remote target...
Reading /usr/lib/aarch64-linux-gnu/tegra/.debug/libnvos.so.debug from remote target...
```

Following <a href="https://devtalk.nvidia.com/default/topic/1049619/jetson-tx2/debugging-cross-compiled-cuda-sample-does-not-work-/" target="blank">this</a> and <a href="https://devtalk.nvidia.com/default/topic/1052253/jetson-agx-xavier/no-timeline-for-profiler-xavier-nvvp-and-nsight-compute-is-not-working-in-jetson-xavier/post/5347671/#5347671" target="blank">this</a> discussion, one need to execute the following three steps in Xavier's Linux environment.

1. Setup root password

```bash
$ sudo passwd root
```

2. Edit /etc/ssh/sshd_config, add the following lines to allow root login

```
PermitRootLogin yes
PermitEmptyPasswords no
```

3. Reboot

```bash
$ sudo reboot
```

## Run Profiler
Connect Xavier to the host PC via a usb-c wire, the wire should be connected to Xavier's port next to the LED power indicator. Check Xavier's ip address by either checking its network configuration in Xavier's environment or by running `ifconfig` on the host PC. Then follow <a href="https://blog.csdn.net/c406495762/article/details/70912947" target="blank">this post</a> to setup a project. Note that the user name for the remote connection should be "root" instead of "ubuntu".

With all these set up, one can make a simple cuda project and run the profiler to get the profiling result.