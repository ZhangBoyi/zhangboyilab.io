---
title: Calling C++ in Swift
date: 2018-05-05
tags: ["Swift", "iOS", "C++"]
---

I've been developing an iOS app with image processing capability. The app is programmed in Swift 4, yet the image processing library is written in C++ for performance and compatibility concerns. Therefore there is a need to bridge C++ class to Swift.

Since Swift cannot call C++ directly yet, we need an Objective-C wrapper (more specifically, an Objective-C++ wrapper) as the middleman. Therefore we will first talk about how to call Objective-C function from Swift, and then we'll talk about how to call C++ from Objective-C++.

## Calling Objective-C from Swift

Forget about C++, let's assume you have been able to connect C++ with Objective-C perfectly, or your library is purely written in Objective-C, so now your only concern is calling Objective-C classes and functions from Swift.

I assume you already have an app written in Swift running, if not, you can follow [this post](https://zhangboyi.gitlab.io/post/2018-05-01-starting-an-app-in-swift/) to get a minimalistic app running.

To create an Objective-C class, select _File -> New -> File..._. In the dialog that appears, choose _Objective-C File_ and click _Next_.

![New Objective-C File](/post_img/2018-05-05-calling-cpp-in-swift/1.png "New Objective-C File")

Let's name the new file "ClassOc", and click _Next_. In the dialog that follows, choose a directory to store the file. You may simply use the default, and click _Creat_. Then you'll be prompted to create an Objective-C bridging header. 

![Create Bridging Header](/post_img/2018-05-05-calling-cpp-in-swift/2.png "Create Bridging Header")

Click _Create Bridging Header_, and on the left side of your Xcode IDE, you can see two files created, _ClassOc.m_ and _SingleViewPrj-Bridging-Header.h_. The first part of the bridging header's name is the project's name. 

The bridging header informs the Swift side what Objective-C classes, functions, and variables are available. Let's finish up implementing our Objective-C class first before we get back to the bridging header.

Similar to the procedure of creating the Objective-C class, create a header file for the class by selecting _File -> New -> File..._, and choosing _Header File_. We name the header file "ClassOc.h"

We define a simple class in Objective-C as follows:

_ClassOc.h_

```objc
#ifndef ClassOc_h
#define ClassOc_h

#import <Foundation/Foundation.h>

@interface ClassOc : NSObject

@property NSString *myString;

- (void) printString;

- (void) updateString;

@end

#endif /* ClassOc_h */
```

_ClassOc.m_

```objc
#import <Foundation/Foundation.h>
#import "ClassOc.h"

// implementation of the objective-c class
@implementation ClassOc

- (instancetype)init {
    self = [super init];
    if (self) {
        self.myString = @"ClassOc is working!";
    }
    return self;
}

- (void) printString {
    NSLog(@"%@", self.myString);
}

- (void) updateString {

}

@end
```

This class simply prints its string property as a log. To let the Swift know the existance of this class, we add line <code class="objc">#import "ClassOc.h"</code> to _SingleViewPrj-Bridging-Header.h_.

![Implement Bridging Header](/post_img/2018-05-05-calling-cpp-in-swift/3.png "Implement Bridging Header")

We update the <code class="swift">viewDidLoad()</code> function in _VidwController.swift_ as follows to call the Objective-C function.

```swift
override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    NSLog("View Loaded");
    
    let classOc = ClassOc();
    classOc.updateString();
    classOc.printString();
}
```

Build and run, you should see _"ClassOC is Working"_ printed in the log as shown in the previous figure. Now you have successfully implemented an Objective-C class and bridged it to Swift.

## Calling C++ from Objective-C

In this section, we'll focus on calling C++ from Objective-C. Here Swift is irrevalent, this section is applicable if you are writing an app purely in Objective-C and C++.

We first add _ClassCpp.cpp_ and its header _ClassCpp.hpp_ to the project following a similar procedure as we added _ClassOc.m_. We write our C++ class as follows.

_ClassCpp.cpp_

```cpp
#include "ClassCpp.hpp"
#include <string>

using namespace std;

ClassCpp::ClassCpp() {
    myStringCpp = "ClassCpp's string is passed";
}

string ClassCpp::getString() {
    return myStringCpp;
}
```
_ClassCpp.hpp_

```cpp
#ifndef ClassCpp_hpp
#define ClassCpp_hpp

#include <stdio.h>
#include <string>

using namespace std;

class ClassCpp {
public:
    ClassCpp();
    string getString();
private:
    string myStringCpp;
};

#endif /* ClassCpp_hpp */
```

This class has a <code class="cpp">getString()</code> method which returns a C++ string. To call this class in Objective-C, we update _ClassOc.m_ as follows, with the changes highlighted in yellow.

<pre><code class="objc">#import &lt;Foundation/Foundation.h&gt;
#import "ClassOc.h"
<mark>#import "ClassCpp.hpp"</mark>

<mark>ClassCpp* classCpp;</mark>

// implementation of the objective-c class
@implementation ClassOc

- (instancetype)init {
    self = [super init];
    if (self) {
        self.myString = @"ClassOc is working!";
        <mark>classCpp = new ClassCpp();</mark>
    }
    return self;
}

- (void) printString {
    NSLog(@"%@", self.myString);
}

- (void) updateString {
    <mark>std::string cppString = classCpp->getString();</mark>
    <mark>self.myString = [NSString stringWithUTF8String:cppString.c_str()];</mark>
}

@end
</code></pre>

If you build and run from here, a compiler error would occur, because _ClassOc.m_ is only compiled as C and Objective-C file. The compiler cannot understand C++ syntax, neigher can it find C++ files like <code class="cpp">&lt;string&gt;</code>. To fix this, we rename the file as _ClassOc.mm_. Here, _*.mm_ file is called an Objective-C++ file, it signals the compiler to compile it as both Objective-C and C++.

Build and run, you will see _“ClassCpp's string is passed"_ printed in the log. This string is passed from C++ to Objective-C. This shows we have successfully linked C++ to Objective-C, and to Swift. 

## Final notes

In case you did not add the bridging-header automatically, or you want to add the bridging-header manually, add the .h file, and modify the settings as shown in the following figure.

![Bridging Header](/post_img/2018-05-05-calling-cpp-in-swift/4.png "Bridging Header")

Sometimes when compiling C++ with Objective-C, you receive an error like the following.

```
fatal error: ...: can't open input file: <string> (No such file or directory)
```

Yet you know the C++ header file is present, and maybe you can even open it in the IDE. If this error occurs, usually it is because you are calling C++ from a file other than _.mm_. For example, if I put <code class="cpp">#import "ClassCpp.hpp"</code> in _ClassOc.h_, I'll get that error. This is because the compiler takes _ClassOc.h_ as Objective-C code, and as it tries to find _&lt;string&gt;_ in the directory for C headers, it fails to find it. 

The solution, of course, is to only have C++ code in _.mm_ file, and keep the rest of Objective-C files pure from C++.
