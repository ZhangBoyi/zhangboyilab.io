---
title: Install Tensorflow Keras Tvm On Xavier
date: 2020-02-21
tags: ["CUDA", "Linux"]
---
This blog talks about how to run and install TVM, Tensorflow, and Keras on Jetson Xavier with Jetpack 4.3. The steps are

* __Install TVM__
* __Install Tensorflow__
* __Install Keras__

## Install TVM
TVM is an engine that compiles and optimizes deep learning model for different hardware platforms. To install is on Xaveir, run the following commands adapted from <a href="https://docs.tvm.ai/install/from_source.html" target="blank">this tutorial</a>. 

```bash
$ git clone --recursive https://github.com/apache/incubator-tvm tvm
$ sudo apt-get update
$ sudo apt-get install -y python3 python3-dev python3-setuptools gcc libtinfo-dev zlib1g-dev build-essential cmake libedit-dev libxml2-dev
$ mkdir build
$ cp cmake/config.cmake build
$ vim build/config.cmake  # turn on set(USE_CUDA ON), set(USE_CUDNN ON), and set(USE_LLVM ON)

# build LLVM with the following commands adapted from the build section of https://lld.llvm.org/
$ git clone https://github.com/llvm/llvm-project llvm-project
$ git checkout release/9.x
$ cd llvm-project
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_PROJECTS=lld -DCMAKE_INSTALL_PREFIX=/usr/local ../llvm
$ sudo make -j6 install

# build tvm
$ cd build
$ cmake -DLLVM_DIR=/home/raps/Documents/llvm-project/build/lib/cmake/llvm -DBUILD_TYPE=Release ../  # specify path to LLVM cmake config
$ make -j6

# edit .bashrc
$ vim ~/.bashrc
# add the following two lines to .bashrc
# export TVM_HOME=/path/to/tvm
# export PYTHONPATH=$TVM_HOME/python:$TVM_HOME/topi/python:${PYTHONPATH}

# install python dependencies
$ pip3 install --user cython  # running pip3 install numpy will fail, because cython cannot be installed automatically for arm64 processor
$ pip3 install --user numpy decorator attrs
```

## Install Tensorflow
Follow <a href="https://devtalk.nvidia.com/default/topic/1042125/jetson-agx-xavier/official-tensorflow-for-jetson-agx-xavier/" target="blank">this post</a> to install tensorflow. The commands are

```bash
$ sudo apt-get install libhdf5-serial-dev hdf5-tools libhdf5-dev zlib1g-dev zip libjpeg8-dev
$ sudo apt-get install python3-pip
$ sudo pip3 install -U pip
$ sudo pip3 install -U numpy grpcio absl-py py-cpuinfo psutil portpicker six mock requests gast h5py astor termcolor protobuf keras-applications keras-preprocessing wrapt google-pasta

# if we install tensorflow directly, it will report this error: ERROR: tensorboard 1.15.0 has requirement setuptools>=41.0.0, but you'll have setuptools 39.0.1 which is incompatible.
# therefore we run the following commands to update setuptools first
$ sudo pip3 uninstall setuptools
$ sudo apt install python3-testresources
$ sudo pip3 install --ignore-installed setuptools==41.0.0

$ sudo pip3 install --pre --extra-index-url https://developer.download.nvidia.com/compute/redist/jp/v43 tensorflow-gpu==1.15.0+nv20.1
```

## Install Keras
The commands are
```bash
# note that installing scipy using pip3 would fail on arm64. That's why we use apt install for scipy.
$ sudo apt-get install -y build-essential libatlas-base-dev gfortran python3-scipy
$ sudo pip3 install keras  
```

As a side note, to check the installed jetpack version, use command `cat /etc/nv_tegra_release`
