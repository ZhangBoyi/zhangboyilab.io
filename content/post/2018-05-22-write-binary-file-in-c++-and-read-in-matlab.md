---
title: Write Binary File In C++ And Read In MATLAB
date: 2018-05-22
tags: ["MATLAB", "C++"]
---

Following [this post](http://www.orangeowlsolutions.com/archives/241), I'm able to write binary file in C++ to pass data to MATLAB.

In C++, I have a <code class="c++">cv::Mat matFloats</code> of type float, I used the following code to store the data in a binary file.

```c++
#include <fstream>

ofstream outfile;
outfile.open("data.dat", std::ios::out | std::ios::binary);
outfile.write((char*)matFloat.data, 2 * contourCentered.size() * sizeof(float));
outfile.close();
```

And the following code in MATLAB reads the data and reshapes the array.

```matlab
fd = fopen('data.dat', 'r');
matFloat = fread(fd, 'float');
fclose(fd);

matFloat = reshape(matFloat, 2, []);
```



